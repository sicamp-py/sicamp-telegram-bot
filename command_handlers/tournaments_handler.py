from typing import Union

from telebot import TeleBot
from telebot.types import Message

import helpers
import keyboard_helper
from access import AccessLevel
from buttons import ButtonType
from persistent_storage.tournaments_repository import TournamentsRepository, TournamentData
from persistent_storage.users_repository import UsersRepository
from states import State
from tournament.tournaments import tournament_transitions, descriptions, tournament_infos
import command_parser as parser
from tournament import registration


class TournamentsHandler:
    def __init__(self, bot: TeleBot, tournaments: TournamentsRepository, keyboard_sender: keyboard_helper.KeyboardSender, users: UsersRepository,):
        self.bot = bot
        self.tournaments = tournaments
        self.keyboard_sender = keyboard_sender
        self.users = users

    def keyboard_in_tournaments(self, message: Message):
        keyboard = keyboard_helper.create_tournaments_keyboard()
        tournament_list = self.get_list_of_tournaments()
        self.keyboard_sender.send_keyboard(message.chat.id, tournament_list, keyboard)

    @staticmethod
    def get_list_of_tournaments():
        buttons = enumerate([ButtonType.Soccer, ButtonType.Hat, ButtonType.Poker])
        formatted_buttons = '\n'.join([helpers.make_bold(str(i + 1) + '. ') + button.value for i, button in buttons])
        return '*Турниры:*\n' + formatted_buttons

    def handle_tournaments(self, message: Message) -> Union[State, None]:
        button_type = parser.try_parse_button(message.text)
        if button_type is None:
            return None
        next_state = tournament_transitions[button_type]
        if button_type == ButtonType.Back:
            self.keyboard_sender.send_hide_keyboard(message)
            return State.MAIN
        else:
            keyboard = keyboard_helper.create_team_list_keyboard()
            self.keyboard_sender.send_keyboard(message.chat.id, descriptions[next_state], keyboard)
        return next_state

    def handle_concrete_tournament(self, message: Message, state: State) -> State:
        button_type = parser.try_parse_button(message.text)
        user_info = self.users.read(message.from_user.id)
        show_ids = user_info.access_level.value >= AccessLevel.PREP.value
        if button_type is None:
            tournament_info = tournament_infos[state]
            tournament_data = registration.try_parse_tournament_data(message.text, tournament_info)
            if tournament_data is not None:
                self.tournaments.save(message.from_user.id, state, tournament_data)
                self.bot.send_message(message.chat.id, 'Регистрация прошла успешно')
                self.keyboard_in_tournaments(message)
                return State.TOURNAMENTS
            else:
                self.bot.send_message(message.chat.id, 'Форма заполнена некорректно, попробуй еще раз')
                self.bot.send_message(message.chat.id, descriptions[state], parse_mode="Markdown")
                return state
        else:
            if button_type == ButtonType.Back:
                self.keyboard_in_tournaments(message)
                return State.TOURNAMENTS
            if button_type == ButtonType.TeamList:
                tournament_type = ''
                if state == State.SOCCER:
                    tournament_type = 'SOCCER'
                if state == State.POKER:
                    tournament_type = 'POKER'
                elif state == State.HAT:
                    tournament_type = 'HAT'
                text = self._format_list_of_teams(tournament_type, show_ids)
                self.bot.send_message(message.chat.id, text)
                return state

    def delete_team(self, tournament_type: str, object_id: str) -> bool:
        teams = self.tournaments.read_all(tournament_type)
        team_exists = any([team.team_id == object_id for team in teams])
        if team_exists:
            self.tournaments.delete(tournament_type, object_id)
        return team_exists

    def _format_list_of_teams(self, tournament_type: str, show_ids):
        teams = self.tournaments.read_all(tournament_type)
        if not teams:
            return 'Пока ничего нет'
        return '\n\n'.join([self._format_team(team, i + 1, show_ids) for i, team in enumerate(teams)])

    @staticmethod
    def _format_team(team: TournamentData, index: int, show_ids):
        if show_ids:
            if team.team_name:
                return '\n'.join([str(index) + '. ' + "id: " + team.team_id + "\n" + team.team_name] + team.members)
            return str(index) + '. ' + "id: " + team.team_id + "\n" + '\n'.join(team.members)
        if team.team_name:
            return '\n'.join([str(index) + '. ' + team.team_name] + team.members)
        return str(index) + '. ' + '\n'.join(team.members)

