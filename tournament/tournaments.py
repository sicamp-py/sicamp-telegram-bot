from typing import Callable, Union

from buttons import ButtonType
from states import State

descriptions = {
    State.HAT: """*Заполни форму для регистрации команды:*
Напиши в следующих 3 строках:
  - ФИ первого участника
  - ФИ второго участника
  - 20 загаданных вами слов через пробел
  """,
    State.SOCCER: """*Заполни форму для регистрации команды:* 
  - Название вашей команды:
  - ФИ первого участника
  - ФИ второго участника
  - ФИ третьего участника
  - ФИ четвертого участника
  - ФИ пятого участника
  - ФИ шестого участника
    """,
     State.POKER: """*Заполни форму для регистрации:*
  - ФИ участника
    """,
}

tournament_transitions = {
    ButtonType.Soccer: State.SOCCER,
    ButtonType.Hat: State.HAT,
    ButtonType.Poker: State.POKER,
    ButtonType.Back: State.MAIN,
}


class TournamentInfo:
    def __init__(self, has_team_name: bool, team_size: int, check_additional_info: Union[Callable, None]):
        self.has_team_name = has_team_name
        self.team_size = team_size
        self.check_additional_info = check_additional_info


def check_hat_info(info: str):
    return len(info.split()) == 20

soccer_info = TournamentInfo(has_team_name=True, team_size=6, check_additional_info=None)
hat_info = TournamentInfo(has_team_name=False, team_size=2, check_additional_info=check_hat_info)
poker_info = TournamentInfo(has_team_name=False, team_size=1, check_additional_info=None)


tournament_infos = {
    State.SOCCER: soccer_info,
    State.HAT: hat_info,
    State.POKER: poker_info,
}