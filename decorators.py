from typing import Callable

from telebot.types import Message

from command_handler import CommandHandler
from persistent_storage.users_repository import UsersRepository
from persistent_storage.user_info import UserInfo


def for_existing_users(users: UsersRepository, command_handler: CommandHandler):
    def decorator(handler: Callable[[Message, UserInfo], None]):
        def wrapped_handler(message: Message):
            user_id, username = message.from_user.id, message.from_user.username
            if not users.exists(user_id):
                command_handler.send_text_for_unregistered_person(message)
                return

            users.update_nickname(user_id, username)
            user_info = users.read(user_id)
            return handler(message, user_info)

        return wrapped_handler

    return decorator
