import os
from time import sleep
import sys

from requests import ReadTimeout
from telebot import TeleBot, logger, apihelper
from telebot.types import Message

import decorators
from cat_api.catapi import CatApi
from command_handler import CommandHandler
from command_handlers.tournaments_handler import TournamentsHandler
from keyboard_helper import KeyboardSender
from persistent_storage.lost_and_found_repository import ItemsRepository
from persistent_storage.schema_actualizer import SchemaActualizer
from persistent_storage.tournaments_repository import TournamentsRepository
from persistent_storage.users_repository import UsersRepository
from states import State
from access import AccessLevel
from persistent_storage.user_info import UserInfo
import schedule.schedule_parser as schedule_parser
import bot_logging
from persistent_storage.cassandra_manager import CassandraManager

use_proxy = '--proxy' in sys.argv

token = os.environ["BOT_TOKEN"]
superuser_id = int(os.environ["SUPERUSER_ID"])

cassandra_manager = CassandraManager()
schema_actualizer = SchemaActualizer(cassandra_manager)
schema_actualizer.actualize('bot_keyspace')

if use_proxy:
    apihelper.CONNECT_TIMEOUT = 10.0
    apihelper.proxy = {"https": "socks5h://telegram:telegram@ogyom.tgvpnproxy.me:1080"}

bot = TeleBot(token, threaded=False)
bot_logging.set_up()

schedule_path = "schedule/schedule.txt"
events = schedule_parser.get_events_from_file(schedule_path)

tournaments = TournamentsRepository("persistent_storage/storage/tournaments")
users = UsersRepository(cassandra_manager)
lost_items = ItemsRepository("persistent_storage/storage/lost_and_found", "lost_items.txt")
found_items = ItemsRepository("persistent_storage/storage/lost_and_found", "found_items.txt")

keyboard_sender = KeyboardSender(bot, users)
tournaments_handler = TournamentsHandler(bot, tournaments, keyboard_sender, users)
handler = CommandHandler(bot,
                         CatApi(),
                         events,
                         tournaments_handler,
                         keyboard_sender,
                         users,
                         lost_items,
                         found_items)

superuser_info = UserInfo(user_id=superuser_id, state=State.MAIN, access=AccessLevel.SUPERUSER)
users.save(superuser_info)

for_existing_users = decorators.for_existing_users(users, handler)


@bot.message_handler(commands=['start'])
def start_handler(message: Message):
    user_id, username = message.from_user.id, message.from_user.username
    logger.info("New user with id: %d, username: %s", user_id, username)
    if not users.exists(user_id):
        user_info = UserInfo(user_id, state=State.MAIN, access=AccessLevel.READ, nickname=username)
        users.save(user_info)
        handler.start(message)
    else:
        help_handler(message)


@bot.message_handler(commands=['help'])
@for_existing_users
def help_handler(message: Message, user_info: UserInfo):
    handler.help(message)


@bot.message_handler(commands=['cats'])
@for_existing_users
def cats_handler(message: Message, user_info: UserInfo):
    handler.send_cats(message)


@bot.message_handler(commands=['schedule'])
@for_existing_users
def schedule_handler(message: Message, user_info: UserInfo):
    handler.get_schedule(message)


@bot.message_handler(commands=['next_event'])
@for_existing_users
def next_event_handler(message: Message, user_info: UserInfo):
    handler.get_next_event(message)


@bot.message_handler(commands=['tournaments'])
@for_existing_users
def send_list_of_tournaments(message: Message, user_info: UserInfo):
    if user_info.access_level.value < AccessLevel.WRITE.value:
        bot.send_message(message.chat.id, "У вас нет прав!")
        return
    users.change_state(user_info.user_id, state=State.TOURNAMENTS)
    tournaments_handler.keyboard_in_tournaments(message)


@bot.message_handler(commands=['items'])
@for_existing_users
def send_lost_and_found_keyboard(message: Message, user_info: UserInfo):
    users.change_state(user_info.user_id, State.ITEMS)
    handler.send_keyboard_in_items(message)


@bot.message_handler(commands=['delete'])
@for_existing_users
def send_delete_item(message: Message, user_info: UserInfo):
    logger.info("/delete from %s: %s", user_info.nickname, message.text)
    handler.delete_item(message)


@bot.message_handler(commands=['set_name'])
@for_existing_users
def set_name(message: Message, user_info: UserInfo):
    if user_info.access_level.value >= AccessLevel.PREP.value:
        logger.info("/set_name from %s: %s", user_info.nickname, message.text)
        handler.set_name(message)
    else:
        bot.send_message(message.chat.id, "У вас нет прав!")


@bot.message_handler(commands=['give_access'])
@for_existing_users
def change_access_level(message: Message, user_info: UserInfo):
    if user_info.access_level.value < AccessLevel.PREP.value:
        bot.send_message(message.chat.id, "У вас нет прав!")
        return
    logger.info("/give_access from %s: %s", user_info.nickname, message.text)
    handler.give_access(message)


@bot.message_handler(commands=['access_level'])
@for_existing_users
def get_access_level_list(message: Message, user_info: UserInfo):
    if user_info.access_level.value < AccessLevel.PREP.value:
        bot.send_message(message.chat.id, "У вас нет прав!")
        return
    handler.get_access_level_list(message)


@bot.message_handler(commands=['print_info'])
@for_existing_users
def print_info(message: Message, user_info: UserInfo):
    if user_info.access_level.value >= AccessLevel.PREP.value:
        handler.print_info(message)
    else:
        bot.send_message(message.chat.id, "У вас нет прав!")


@bot.message_handler(commands=['print_levels'])
@for_existing_users
def print_levels(message: Message, user_info: UserInfo):
    if user_info.access_level.value >= AccessLevel.PREP.value:
        handler.print_levels(message)
    else:
        bot.send_message(message.chat.id, "У вас нет прав!")


@bot.message_handler(commands=['advert'])
@for_existing_users
def print_advert(message: Message, user_info: UserInfo):
    if user_info.access_level.value >= AccessLevel.PREP.value:
        handler.make_advert(message)
    else:
        bot.send_message(message.chat.id, "У вас нет прав!")


@bot.message_handler(func=lambda message: True, content_types=['text'])
@for_existing_users
def default_handler(message: Message, user_info: UserInfo):
    logger.info("Text from user %s: %s", user_info.nickname or str(user_info.user_id), message.text)
    next_state = handler.handle_default(message, user_info.state)
    if next_state is None:
        help_handler(message)
    else:
        users.change_state(user_info.user_id, state=next_state)


running = True
while running:
    try:
        bot.polling(none_stop=True)
        running = False
    except ReadTimeout as e:
        logger.warning("Read timeout: " + str(e))
        sleep(10)
