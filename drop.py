import argparse
import os
from typing import List


def parse_folders() -> List[str]:
    parser = argparse.ArgumentParser(description='Drop records in persistent storage.')
    parser.add_argument('folders', metavar='folder', type=str, nargs='+',
                        help='folders of file storage to drop')
    return parser.parse_args().folders


if __name__ == "__main__":
    folders = parse_folders()
    base_path = 'persistent_storage/storage'
    for folder in folders:
        if not os.path.exists('/'.join([base_path, folder])):
            print('No such folder: ' + folder)
            exit(0)
    print('Dropping folders: ' + ' '.join(folders))
    for folder in folders:
        path = '/'.join([base_path, folder])
        for entity in os.listdir(path):
            path_to_entity = path + '/' + entity
            if entity != '.gitignore' and os.path.isfile(path_to_entity):
                os.remove(path_to_entity)
        print('Successfully dropped folder: ' + folder)