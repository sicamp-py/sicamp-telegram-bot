from enum import Enum


class State(Enum):
    START = 0
    MAIN = 1
    TOURNAMENTS = 2
    SOCCER = 3
    HAT = 4
    POKER = 5
    ITEMS = 6
    LOST = 7
    FOUND = 8
    MY_LOST_ITEM = 9
    MY_FOUND_ITEM = 10


    @staticmethod
    def from_int(value: int):
        if value not in State._value2member_map_:
            raise ValueError("Not value of enum 'State'")
        return State._value2member_map_[value]