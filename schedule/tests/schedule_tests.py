import os
import pytest
import schedule.schedule_formatter as schedule
import schedule.schedule_parser as parser


@pytest.fixture
def scedule_path() -> str:
    return os.path.dirname(os.path.abspath(__file__)) + "\\schedule_test.txt"


def test_next_event_correct(scedule_path):
    events = parser.get_events_from_file(scedule_path)
    expected = """📣08:45 - Линейка\n"""
    assert schedule.get_next_event(events, "08:27") == expected


def test_next_event_after_sleeptime(scedule_path):
    events = parser.get_events_from_file(scedule_path)
    expected = """*---------- Текущее время - 23:05 ----------*
Ты что не спишь?))) Бегом спать!!! Уже жесткий отбой!!!!\n"""
    assert schedule.get_next_event(events, "23:05") == expected


def test_schedule_correct_with_currenttime_between(scedule_path):
    events = parser.get_events_from_file(scedule_path)
    expected = """📋📋📋📋📋📋📋📋📋📋
🔊08:00 - Подъём
🏃08:20 - Зарядка
📣08:45 - Линейка
*---------- Текущее время - 08:47 ----------*
☕09:00 - Завтрак I смены
☕09:30 - Завтрак II смены
☕09:30 - Завтрак II смены
📖10:00 - I Пара II смены\n\n"""
    assert schedule.get_schedule(events, "08:47") == expected


def test_schedule_correct_with_currenttime_overhead(scedule_path):
    events = parser.get_events_from_file(scedule_path)
    expected = """📋📋📋📋📋📋📋📋📋📋
*---------- Текущее время - 07:47 ----------*
🔊08:00 - Подъём
🏃08:20 - Зарядка
📣08:45 - Линейка
☕09:00 - Завтрак I смены
☕09:30 - Завтрак II смены
☕09:30 - Завтрак II смены
📖10:00 - I Пара II смены

"""
    assert schedule.get_schedule(events, "07:47") == expected


def test_next_event_correct_with_similar_time(scedule_path):
    events = parser.get_events_from_file(scedule_path)
    expected = """☕09:30 - Завтрак II смены
☕09:30 - Завтрак II смены
"""
    assert schedule.get_next_event(events, "09:20") == expected


def test_schedule_after_sleeptime(scedule_path):
    events = parser.get_events_from_file(scedule_path)
    expected = """Ты что не спишь?))) Бегом спать!!! Уже жесткий отбой!!!!
"""
    assert schedule.get_schedule(events, "20:47") == expected

