from itertools import takewhile
from typing import Tuple, List

import helpers

request_after_sleeptime = "Ты что не спишь?))) Бегом спать!!! Уже жесткий отбой!!!!\n"


def get_schedule(events: List[Tuple], current_time: str) -> str:
    header = u'\U0001F4CB' * 10 + "\n"
    events_before_time = _get_current_event_index(events, current_time)
    events_messages = [e[1] for e in events]
    if len(events_messages[events_before_time:]) != 0:
        all_events = [header] \
            + events_messages[:events_before_time] \
            + [_format_current_time(current_time)] \
            + events_messages[events_before_time:]
        return ''.join(all_events)
    else:
        return request_after_sleeptime


def get_next_event(events: List[Tuple], current_time: str) -> str:
    event_index = _get_current_event_index(events, current_time)
    if event_index >= len(events):
        return _format_current_time(current_time) + request_after_sleeptime
    else:
        i = 0
        next_event = []
        while events[event_index][0] == events[event_index + i][0]:
            next_event.append(events[event_index + i][1])
            i += 1
        return ''.join(next_event)


def _get_current_event_index(events: List[Tuple], current_time: str) -> int:
    return len(list(takewhile(lambda x: x[0] < current_time, events)))


def _format_current_time(current_time: str, len_segment: int=10) -> str:
    separator = "-" * len_segment
    time_layout = " Текущее время - " + current_time + " "
    return helpers.make_bold(separator + time_layout + separator) + '\n'
