import logging
from logging.handlers import RotatingFileHandler

from telebot import logger


def set_up():
    log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
    log_file = 'log.txt'
    rotating_file_handler = RotatingFileHandler(log_file, mode='a', maxBytes=5*1024*1024,
                                     backupCount=2, encoding='utf-8', delay=False)
    rotating_file_handler.setFormatter(log_formatter)
    rotating_file_handler.setLevel(logging.INFO)

    logger.setLevel(logging.INFO)
    logger.addHandler(rotating_file_handler)
