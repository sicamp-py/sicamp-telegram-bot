from datetime import datetime

from access import AccessLevel
from states import State

COMMANDS = [
    ('/help', 'Выдает список доступных команд'),
    ('/cats <число котиков>', 'Получить n котиков'),
    ('/schedule', 'Получить расписание на сегодня'),
    ('/next_event', 'Получить следущее событие в расписании'),
    ('/tournaments', 'Запись на турниры'),
    ('/items', 'Потеряшки и находки'),
    ('/delete', 'Удалить свою потеряшку/находку')
]

PRIVATE_COMMANDS = [
    ('/give_access <username> <уровень доступа>', 'Дать уровень доступа школьнику'),
    ('/set_name <username> <фамилия> <имя>', 'Занести соответсвие имени школьника и его никнейма в телеграме'),
    ('/print_info <username>', 'Вывести информацию о пользователе'),
    ('/print_levels', 'Вывести список уровней доступа'),
    ('/advert', 'Отправить сообщение всем пользователям'),
    ('/access_level <AccessLevel>', 'Узнать пользователей с таким уровнем доступа')
]


def make_bold(s):
    return "*" + s + "*"


def current_time_less_than_given_time(time):
    return get_current_time() <= time


def get_current_time():
    curtime = datetime.now()
    return datetime.strftime(curtime, "%H:%M")


def is_concrete_tournament(state: State) -> bool:
    return state in [State.SOCCER, State.HAT, State.POKER]


def get_help_text(users, user_id):
    text = '\n'.join(["{0}: {1}".format(key, value) for (key, value) in COMMANDS])
    info = users.read(user_id)
    if info.access_level.value >= AccessLevel.PREP.value:
        text += '\n' + '\n'.join(["{0}: {1}".format(key, value) for (key, value) in PRIVATE_COMMANDS])
    return text
