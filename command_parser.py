from typing import Union, Tuple

from access import AccessLevel
from buttons import ButtonType
from states import State


def get_username(text: str) -> Union[str, None]:
    try:
        return text.split()[1]
    except IndexError:
        return None


def get_cats_args(message_text):
    args = message_text.split()
    return int(args[1])


def get_text_args(message_text : str) -> str:
    return message_text[len("/advert"):].strip()


def get_set_name_args(message_text) -> Tuple:
    args = message_text.split()
    if len(args) < 4:
        return None, None
    username = args[1]
    new_name = str(args[2] + ' ' + args[3])
    return username, new_name


def get_access(message_text: str) -> Tuple:
    args = message_text.split()
    if len(args) < 3:
        return None, None
    username = args[1]
    try:
        access_level = AccessLevel.from_string(args[2])
    except ValueError:
        return None, None
    return username, access_level


def get_access_level_for_list(message_text: str) -> Union[AccessLevel, None]:
    args = message_text.split()
    if len(args) < 2:
        return None
    else:
        return try_parse_access_level(args[1])


def get_delete_args(message_text):
    args = message_text.split()
    return args[1], args[2]


def try_parse_button(text: str) -> Union[State, None]:
    try:
        return ButtonType.from_string(text)
    except ValueError:
        return None


def try_parse_access_level(text: str) -> Union[AccessLevel, None]:
    try:
        return AccessLevel.from_string(text)
    except ValueError:
        return None