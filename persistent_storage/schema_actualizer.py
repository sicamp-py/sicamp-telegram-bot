from persistent_storage.cassandra_manager import CassandraManager


class SchemaActualizer:
    def __init__(self, cassandra_manager: CassandraManager):
        self._session = cassandra_manager.get_session()

    def actualize(self, keyspace_name):
        self._session.execute(create_keyspace % keyspace_name)
        self._session.execute(use_keyspace % keyspace_name)

        self._session.execute(create_users_by_id)
        self._session.execute(create_users_by_nickname)


create_keyspace = """
CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}
"""

use_keyspace = "USE %s"

create_users_by_id = """
CREATE TABLE IF NOT EXISTS users_by_id (user_id varint, state text, access_level text, nickname text, name text, 
PRIMARY KEY (user_id))
"""

create_users_by_nickname = """
CREATE TABLE IF NOT EXISTS users_by_nickname (nickname text, user_id varint, PRIMARY KEY (nickname))
"""

