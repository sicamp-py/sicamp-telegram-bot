import uuid
from typing import List

from persistent_storage.file_storage import JsonFileStorage


class ItemData:
    """Information about lost/found item"""

    def __init__(self, text: str,  user_id: str, item_id: str=None):
        if item_id is None:
            self.item_id = str(uuid.uuid4())[:8]
        else:
            self.item_id = item_id
        self.user_id = user_id
        self.info = text


class ItemsRepository:
    """Storage for lost/found items"""

    def __init__(self, path: str, file_name: str):
        self._file_name = file_name
        self._storage = JsonFileStorage(path)

    def save(self, data: ItemData):
        items = self._safe_read_json(self._file_name)
        items.append({
            "user_id": data.user_id,
            "item_id": data.item_id,
            "info": data.info
        })
        self._storage.write(self._file_name, items)

    def delete(self, item_id: str):
        items = self._safe_read_json(self._file_name)
        result_items = [item for item in items if item["item_id"] != item_id]
        self._storage.write(self._file_name, result_items)

    def read_all(self):
        items = self._safe_read_json(self._file_name)
        result = [ItemsRepository._item_data_from_json(item) for item in items]
        return result

    @staticmethod
    def _item_data_from_json(user_json: dict) -> ItemData:
        return ItemData(user_json["info"], user_json["user_id"], user_json["item_id"])

    def _safe_read_json(self, file_id: str) -> List:
        try:
            # noinspection PyTypeChecker
            return list(self._storage.read(file_id))
        except FileNotFoundError:
            return []