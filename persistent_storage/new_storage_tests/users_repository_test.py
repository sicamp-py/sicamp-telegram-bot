from random import randint

import pytest

from access import AccessLevel
from persistent_storage.cassandra_manager import CassandraManager
from persistent_storage.users_repository import UsersRepository
from persistent_storage.schema_actualizer import SchemaActualizer
from persistent_storage.user_info import UserInfo
from states import State


@pytest.fixture(scope='module')
def keyspace_name():
    return 'test_keyspace'


@pytest.fixture(scope='module')
def manager():
    return CassandraManager()


@pytest.fixture(scope='module')
def actualizer(manager):
    return SchemaActualizer(manager)


@pytest.fixture(scope='module', autouse=True)
def before_module(actualizer: SchemaActualizer, keyspace_name):
    actualizer.actualize(keyspace_name)


@pytest.fixture(scope='function', autouse=True)
def before_test(manager):
    manager.get_session().execute("""
    TRUNCATE TABLE users_by_id""")
    manager.get_session().execute("""
        TRUNCATE TABLE users_by_nickname""")


@pytest.fixture()
def storage(manager) -> UsersRepository:
    return UsersRepository(manager)


@pytest.fixture
def user_id() -> int:
    return randint(0, 20000)


@pytest.fixture
def user_info(user_id) -> UserInfo:
    return UserInfo(user_id, State.MAIN, AccessLevel.READ)


def test_write_read_ok(storage: UsersRepository, user_id):
    user_info = UserInfo(user_id, State.MAIN, AccessLevel.WRITE, 'Zzz', 'Qxx Zzz')
    storage.save(user_info)
    assert storage.read(user_info.user_id) == user_info


def test_write_get_by_nickname_ok(storage: UsersRepository, user_id):
    user_info = UserInfo(user_id, State.MAIN, AccessLevel.READ, nickname='Nick')
    storage.save(user_info)
    assert storage.find_user_by_nickname(user_info.nickname) == user_info


def test_exists_ok(storage: UsersRepository, user_info: UserInfo):
    assert not storage.exists(user_info.user_id)
    storage.save(user_info)
    assert storage.exists(user_info.user_id)


def test_not_exists_ok(storage: UsersRepository, user_id: int):
    assert not storage.exists(user_id)


def test_read_raises_when_no_such_user(storage: UsersRepository, user_id: int):
    with pytest.raises(KeyError):
        storage.read(user_id)


def test_find_user_by_nickname_not_found(storage: UsersRepository, user_id: int):
    assert storage.find_user_by_nickname('nickname') is None


def test_change_state(storage: UsersRepository, user_id: int):
    user = UserInfo(user_id, State.START, AccessLevel.READ)
    storage.save(user)

    user.state = State.MAIN
    storage.change_state(user.user_id, user.state)

    assert storage.read(user_id) == user


def test_update_nickname_no_nickname(storage: UsersRepository, user_id: int):
    user = UserInfo(user_id, State.MAIN, AccessLevel.READ, nickname=None)
    storage.save(user)

    user.nickname = 'SomeNick'
    storage.update_nickname(user.user_id, user.nickname)

    assert storage.read(user_id).nickname == user.nickname


def test_update_old_nickname(storage: UsersRepository, user_id: int):
    user = UserInfo(user_id, State.MAIN, AccessLevel.READ, nickname='OldNick')
    storage.save(user)

    user.nickname = 'NewNick'
    storage.update_nickname(user.user_id, user.nickname)

    assert storage.read(user_id).nickname == user.nickname


def test_find_by_nickname_after_update__nickname(storage: UsersRepository, user_id: int):
    user = UserInfo(user_id, State.MAIN, AccessLevel.READ, nickname='OldNick')
    storage.save(user)

    user.nickname = 'NewNick'
    storage.update_nickname(user.user_id, user.nickname)

    assert storage.find_user_by_nickname(user.nickname) == user


def test_read_all(storage: UsersRepository):
    first_user_id = user_id()
    second_user_id = user_id()
    third_user_id = user_id()

    storage.save(UserInfo(first_user_id, State.START, AccessLevel.READ))
    storage.save(UserInfo(second_user_id, State.MAIN, AccessLevel.READ))
    storage.save(UserInfo(third_user_id, State.START, AccessLevel.WRITE))

    users = set([x.user_id for x in storage.read_all()])
    assert users == {first_user_id, second_user_id, third_user_id}
