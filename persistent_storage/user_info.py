from access import AccessLevel
from states import State


class UserInfo:
    def __init__(self,
                 user_id: int,
                 state: State,
                 access: AccessLevel,
                 nickname: [str, None] = None,
                 name: [str, None] = None):
        self.user_id = user_id
        self.state = state
        self.access_level = access
        self.nickname = nickname
        self.name = name

    def __eq__(self, other: 'UserInfo'):
        if type(other) is not UserInfo:
            return False
        return self.user_id == other.user_id and \
               self.state == other.state and \
               self.access_level == other.access_level and \
               self.nickname == other.nickname and \
               self.name == other.name

    # noinspection PyUnresolvedReferences
    @staticmethod
    def from_object(obj: object):
        state = State[obj.state]
        access_level = AccessLevel[obj.access_level]
        return UserInfo(obj.user_id, state, access_level, obj.nickname, obj.name)

    def __str__(self):
        return "User id: {}\nUser name: {}\nName: {}\nAccess_level: {}".format(
            self.user_id, self.nickname, self.name, self.access_level)
