from typing import Iterable, List

from telebot import logger

from persistent_storage.cassandra_manager import CassandraManager
from persistent_storage.user_info import UserInfo
from states import State


class UsersRepository:
    def __init__(self, manager: CassandraManager):
        self._session = manager.get_session()

    def save(self, info: UserInfo):
        self._session.execute("""
        INSERT INTO users_by_id (user_id, state, access_level, nickname, name)
        VALUES (%s, %s, %s, %s, %s)""",
                              (info.user_id,
                               info.state.name,
                               info.access_level.name,
                               info.nickname,
                               info.name))
        if info.nickname:
            self._session.execute("""
            INSERT INTO users_by_nickname (nickname, user_id)
            VALUES (%s, %s)""", (info.nickname, info.user_id))

    def read(self, user_id: int) -> UserInfo:
        users = self._read_by_id(user_id)
        if len(users) == 0:
            logger.error("User %s does not exist", user_id)
            raise KeyError('User does not exist')
        if len(users) > 1:
            logger.error("More than one user found by user_id: %s", user_id)
            raise KeyError('More than one user found')
        return UserInfo.from_object(users[0])

    def exists(self, user_id: int) -> bool:
        users = self._read_by_id(user_id)
        if len(users) > 1:
            logger.error("More than one user found by user_id: %s", user_id)
            raise KeyError('More than one user found')
        return len(users) == 1

    def change_state(self, user_id: int, state: State):
        info = self.read(user_id)
        info.state = state
        self.save(info)

    def update_nickname(self, user_id: int, nickname: str):
        info = self.read(user_id)
        if info.nickname != nickname:
            info.nickname = nickname
            self.save(info)

    def find_user_by_nickname(self, nickname) -> [UserInfo, None]:
        users = self._read_by_nickname(nickname)
        if len(users) == 0:
            return None
        if len(users) > 1:
            logger.error("More than one user found by nickname: %s", nickname)
            raise KeyError('More than one user found')
        user_id = users[0].user_id
        return self.read(user_id)

    def read_all(self) -> Iterable[UserInfo]:
        users = self._session.execute("""
        SELECT user_id, state, access_level, nickname, name FROM users_by_id""")
        return [UserInfo.from_object(obj) for obj in users]

    def _read_by_id(self, user_id: int) -> List[object]:
        return list(self._session.execute("""
        SELECT user_id, state, access_level, nickname, name FROM users_by_id
        WHERE user_id = %s""", (user_id,)))

    def _read_by_nickname(self, nickname: str) -> List[object]:
        return list(self._session.execute("""
        SELECT nickname, user_id FROM users_by_nickname
        WHERE nickname = %s""", (nickname,)))
