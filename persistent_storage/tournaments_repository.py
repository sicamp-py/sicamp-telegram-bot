import uuid
from datetime import datetime
from typing import List, Iterable
from telebot import TeleBot, logger
import helpers
from persistent_storage.file_storage import JsonFileStorage
from states import State


class TournamentData:
    def __init__(self, team_name: str, members: List[str], additional_text: str, team_id: str, timestamp: int):
        self.team_id = team_id
        self.timestamp = timestamp
        self.team_name = team_name
        self.members = members
        self.additional_text = additional_text

    @classmethod
    def create(cls, team_name: str, members: List[str], additional_text: str) -> 'TournamentData':
        team_id = str(uuid.uuid4())[:8]
        timestamp = int(datetime.utcnow().timestamp())
        return cls(team_name, members, additional_text, team_id, timestamp)


class TournamentsRepository:
    def __init__(self, path):
        self._storage = JsonFileStorage(path)

    def save(self, user_id: int, tournament_type: State, data: TournamentData) -> None:
        if not helpers.is_concrete_tournament(tournament_type):
            raise ValueError("State is not concrete tournament")
        file_id = "{}.txt".format(tournament_type.name)
        tournament = self._safe_read_json(file_id)
        tournament.append(TournamentsRepository._to_dict(data, user_id))
        self._storage.write(file_id, tournament)

    @staticmethod
    def _to_dict(data, user_id):
        return {
            "team_id": data.team_id,
            "timestamp": data.timestamp,
            "user_id": user_id,
            "team_name": data.team_name,
            "members": data.members,
            "additional_text": data.additional_text,
        }

    def delete(self, tournament_type: str, team_id: str):
        file_name = tournament_type + ".txt"
        teams = self._safe_read_json(file_name)
        result_tournament = [team for team in teams if team["team_id"] != team_id]
        self._storage.write(file_name, result_tournament)

    def read_all(self, tournament_type: str) -> Iterable[TournamentData]:
        file_name = tournament_type + ".txt"
        teams = self._safe_read_json(file_name)
        result = [TournamentsRepository._tournament_data_from_json(team) for team in teams]
        return result

    def _safe_read_json(self, file_id: str) -> List:
        try:
            # noinspection PyTypeChecker
            return list(self._storage.read(file_id))
        except FileNotFoundError:
            return []

    @staticmethod
    def _tournament_data_from_json(user_json: dict) -> TournamentData:
        return TournamentData(user_json["team_name"],
                              user_json["members"],
                              user_json["additional_text"],
                              user_json["team_id"],
                              user_json["timestamp"])

