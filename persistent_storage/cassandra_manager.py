from cassandra.cluster import Cluster


class CassandraManager:
    def __init__(self):
        self._cluster = Cluster(['127.0.0.1'], port=9043)
        self._session = None

    def get_session(self):
        if self._session is not None:
            return self._session

        self._session = self._cluster.connect()
        return self._session
