from xml.etree import ElementTree as elementTree

from cat_api.image import Image


class CatApiXmlParser:
    """Parse elements from xml representation"""

    def __init__(self, text: str):
        self.root = elementTree.fromstring(text)

    def parse_images(self):
        return list(map(CatApiXmlParser._parse_image, self._find_images_list()))

    def _find_images_list(self):
        return self.root.find('data').find('images')

    @staticmethod
    def _parse_image(element) -> Image:
        return Image(url=element.find('url').text,
                     id=element.find('id').text,
                     source_url=element.find('source_url').text)