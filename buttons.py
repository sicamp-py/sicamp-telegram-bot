from enum import Enum


class ButtonType(Enum):
    Hat = '🎩 Шляпа'
    Back = '⬅ Назад'
    Poker = '🃏 Кочерга'
    Soccer = '⚽ Футбол'
    Lost = '☝ Потеряшки'
    Found = '👍 Находки'
    ILost = '😭 Я потерял'
    LostList = '📋 Список потеряшек'
    IFound = '😃 Я нашел'
    FoundList = '📋 Список находок'
    TeamList = '📋 Список команд'

    @staticmethod
    def from_string(value: str):
        for name in ButtonType._member_names_:
            member = ButtonType._member_map_[name]
            if member.value == value:
                return member
        raise ValueError("Not value of enum 'Smile'")