from typing import List

from telebot import types, TeleBot
from telebot.types import Message

import helpers
from buttons import ButtonType
from persistent_storage.users_repository import UsersRepository


class KeyboardSender:
    def __init__(self, bot: TeleBot, users: UsersRepository):
        self.bot = bot
        self.users = users

    def send_hide_keyboard(self, message: Message):
        self.bot.send_message(message.chat.id, helpers.get_help_text(self.users, message.from_user.id),
                              reply_markup=types.ReplyKeyboardRemove())

    def send_keyboard(self, chat_id: str, text: str, keyboard: types.ReplyKeyboardMarkup):
        self.bot.send_message(chat_id, text, reply_markup=keyboard, parse_mode="Markdown")


def create_keyboard(row_width: int, buttons: List[ButtonType]) -> types.ReplyKeyboardMarkup:
    keyboard = types.ReplyKeyboardMarkup(row_width=row_width, resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(button.value) for button in buttons])
    return keyboard


def create_tournaments_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.Soccer, ButtonType.Hat, ButtonType.Poker, ButtonType.Back]
    return create_keyboard(row_width=3, buttons=buttons)


def create_back_keyboard() -> types.ReplyKeyboardMarkup:
    return create_keyboard(row_width=3, buttons=[ButtonType.Back])


def create_team_list_keyboard() -> types.ReplyKeyboardMarkup:
    return create_keyboard(row_width=3, buttons=[ButtonType.Back, ButtonType.TeamList])


def create_lost_and_found_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.Lost, ButtonType.Found, ButtonType.Back]
    return create_keyboard(row_width=2, buttons=buttons)


def create_lost_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.ILost, ButtonType.LostList, ButtonType.Back]
    return create_keyboard(row_width=2, buttons=buttons)


def create_found_keyboard() -> types.ReplyKeyboardMarkup:
    buttons = [ButtonType.IFound, ButtonType.FoundList, ButtonType.Back]
    return create_keyboard(row_width=2, buttons=buttons)
