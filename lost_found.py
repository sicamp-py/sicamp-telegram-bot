from buttons import ButtonType
from states import State

lost_and_found_texts = {
    ButtonType.ILost: 'Опишите потерянную вещь',
    ButtonType.IFound: 'Опишите найденную вещь',
}

lost_and_found_transitions = {
    ButtonType.ILost: State.MY_LOST_ITEM,
    ButtonType.IFound: State.MY_FOUND_ITEM,
}

