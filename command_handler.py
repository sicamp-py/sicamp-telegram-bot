from typing import Tuple, List, Union

from telebot import TeleBot
from telebot.apihelper import ApiException
from telebot.types import Message

import helpers
import keyboard_helper
from access import AccessLevel
from cat_api.catapi import CatApi
import schedule.schedule_formatter as schedule
from command_handlers.tournaments_handler import TournamentsHandler
from lost_found import lost_and_found_texts, lost_and_found_transitions
from persistent_storage.lost_and_found_repository import ItemData, ItemsRepository
from buttons import ButtonType
from persistent_storage.users_repository import UsersRepository
from states import State
import command_parser as parser


class CommandHandler:
    def __init__(self, bot: TeleBot,
                 cat_api: CatApi,
                 events: List[Tuple[str, str]],
                 tournaments: TournamentsHandler,
                 keyboard: keyboard_helper.KeyboardSender,
                 users: UsersRepository,
                 lost_items: ItemsRepository,
                 found_items: ItemsRepository):
        self.bot = bot
        self.cat_api = cat_api
        self.events = events
        self.tournaments = tournaments
        self.keyboard = keyboard
        self.users = users
        self.lost_items = lost_items
        self.found_items = found_items

    def start(self, message: Message):
        self.bot.send_message(message.chat.id, "Hello! This is SICamp bot!")

    def help(self, message: Message):
        self.bot.send_message(message.chat.id, helpers.get_help_text(self.users, message.from_user.id))

    def send_cats(self, message: Message):
        try:
            images_count = parser.get_cats_args(message.text)
        except ValueError:
            return
        except IndexError:
            images_count = 3
        self._send_images(message.chat.id, self.cat_api.get_cat_images(min(10, images_count)))

    def delete_item(self, message: Message):
        try:
            item_type, item_id = parser.get_delete_args(message.text)
        except IndexError:
            self.send_help_delete_message(message.chat.id)
            return
        if item_type not in ['lost', 'found', 'SOCCER', 'POKER', 'HAT']:
            self.bot.send_message(message.chat.id, 'Неверный формат команды')
            self.send_help_delete_message(message.chat.id)
        elif item_type in ['lost', 'found']:
            if item_type == 'lost':
                storage = self.lost_items
            else:
                storage = self.found_items
            if self._try_delete(storage, item_id):
                self.bot.send_message(message.chat.id, 'Объект удалён')
            else:
                self.bot.send_message(message.chat.id, 'Такого объекта не существует')
        else:
            if self.tournaments.delete_team(item_type, item_id):
                self.bot.send_message(message.chat.id, 'Объект удалён')
            else:
                self.bot.send_message(message.chat.id, 'Такого объекта не существует')

    def send_help_delete_message(self, chat_id: str):
        text = 'Использование: /delete [lost|found|SOCCER|POKER|HAT] [item_id]'
        self.bot.send_message(chat_id, text)

    def get_schedule(self, message: Message):
        current_time = helpers.get_current_time()
        self.bot.send_message(message.chat.id, schedule.get_schedule(self.events, current_time), parse_mode="Markdown")

    def get_next_event(self, message: Message):
        current_time = helpers.get_current_time()
        self.bot.send_message(message.chat.id, schedule.get_next_event(self.events, current_time), parse_mode="Markdown")

    def give_access(self, message: Message):
        changer_id = message.from_user.id
        nickname, access_level = parser.get_access(message.text)
        if nickname is not None and access_level is not None:
            if access_level.value >= self.users.read(changer_id).access_level.value:
                self.bot.send_message(message.chat.id, "У вас нет прав!")
                return
            user_info = self.users.find_user_by_nickname(nickname)
            if user_info is None:
                self.bot.send_message(message.chat.id, "Пользователя {} не существует".format(nickname))
                return
            if user_info.name is None:
                self.bot.send_message(message.chat.id, "Чтобы дать доступ, установите пользователю имя через /set_name")
                return
            user_info.access_level = access_level
            self.users.save(user_info)
            self.bot.send_message(message.chat.id, str(user_info))
        else:
            self.bot.send_message(message.chat.id, "Неверный формат команды или уровень доступа!")

    def get_access_level_list(self, message:Message):
        table = ""
        user_id = message.from_user.id
        access_level = parser.get_access_level_for_list(message.text)
        if access_level is None:
            self.bot.send_message(user_id, "Использование: /access_level <AccessLevel>")
            return
        for info in self.users.read_all():
            if info.access_level == access_level:
                table += (info.nickname or str(info.user_id)) + "\n"
        if table != "":
            self.bot.send_message(user_id, table)
        else:
            self.bot.send_message(user_id, "Список пуст")


    def print_info(self, message: Message):
        username = parser.get_username(message.text)
        if not username:
            self.bot.send_message(message.chat.id, "Использование: /print_info username")
            return
        user_info = self.users.find_user_by_nickname(username)
        if not user_info:
            self.bot.send_message(message.chat.id, "Пользователя {} не существует".format(username))
            return
        self.bot.send_message(message.chat.id, str(user_info))

    def print_levels(self, message: Message):
        levels = [level.name for level in AccessLevel]
        self.bot.send_message(message.chat.id, '\n'.join(levels))

    def set_name(self, message: Message):
        nickname, user_name = parser.get_set_name_args(message.text)
        if user_name is not None and nickname is not None:
            user_info = self.users.find_user_by_nickname(nickname)
            if user_info is None:
                self.bot.send_message(message.chat.id, "Пользователь не найден.")
                return
            user_info.name = user_name
            self.users.save(user_info)
            self.bot.send_message(message.chat.id, str(user_info))
        else:
            self.bot.send_message(message.chat.id, "Использование: /set_name <username> <name> <surname>")

    def send_text_for_unregistered_person(self, message: Message):
        self.bot.send_message(message.chat.id, "Используй /start чтобы начать общение с ботом.")

    def make_advert(self, message: Message):
        advert_text = parser.get_text_args(message.text)
        if not advert_text:
            self.bot.send_message(message.from_user.id, "Пустое сообщение")
        else:
            for info in self.users.read_all():
                try:
                    self.bot.send_message(info.user_id, advert_text)
                except:
                    print("Failed to send message to {}".format(info.nickname or info.user_id))

    def _send_images(self, chat_id: str, images):
        for image in images:
            if image.url.endswith(".gif"):
                method = self.bot.send_document
            else:
                method = self.bot.send_photo
            self._send_image(method, chat_id, image.url)

    def _send_image(self, method, chat_id, url):
        try:
            method(chat_id, url)
        except ApiException:
            self._send_images(chat_id, self.cat_api.get_cat_images(1))

    @staticmethod
    def _try_delete(repository: ItemsRepository, object_id: str) -> bool:
        items = repository.read_all()
        item_exists = any([item.item_id == object_id for item in items])
        if item_exists:
            repository.delete(object_id)
        return item_exists

    @staticmethod
    def _get_total_lines(info):
        return info.has_team_name + info.team_size + (info.check_additional_info is not None)

    def handle_default(self, message: Message, state: State) -> Union[State, None]:
        if state == State.TOURNAMENTS:
            return self.tournaments.handle_tournaments(message)
        elif helpers.is_concrete_tournament(state):
            return self.tournaments.handle_concrete_tournament(message, state)
        elif state == State.ITEMS:
            return self._handle_items(message)
        elif state in [State.LOST, State.FOUND]:
            return self._handle_lost_or_found(message, state)
        elif state in [State.MY_LOST_ITEM, State.MY_FOUND_ITEM]:
            return self._handle_my_found_or_lost_item(message, state)

    def _handle_my_found_or_lost_item(self, message, state: State):
        button_type = parser.try_parse_button(message.text)
        if button_type is None:
            if state == State.MY_FOUND_ITEM:
                storage = self.found_items
            else:
                storage = self.lost_items
            data = ItemData(message.text, message.from_user.id)
            storage.save(data)
            keyboard = keyboard_helper.create_lost_and_found_keyboard()
            self.keyboard.send_keyboard(message.chat.id, CommandHandler._format_items_message(state, data), keyboard)
            return State.ITEMS
        if button_type == ButtonType.Back:
            self.send_keyboard_in_items(message)
            return State.ITEMS

    @staticmethod
    def _format_items_message(state: State, data: ItemData) -> str:
        item = "Потеряшка" if state == State.MY_LOST_ITEM else "Находка"
        return "{} добавлена: {}\nИдентификатор вещи: {}".format(item, data.info, data.item_id)

    def _handle_lost_or_found(self, message, state: State):
        user_info = self.users.read(message.from_user.id)
        button_type = parser.try_parse_button(message.text)
        if button_type is None:
            return None
        if button_type == ButtonType.Back:
            self.send_keyboard_in_items(message)
            return State.ITEMS
        if button_type in [ButtonType.IFound, ButtonType.ILost]:
            if user_info.access_level.value < AccessLevel.WRITE.value:
                self.bot.send_message(message.chat.id, "У вас нет прав на запись!")
                return state
            keyboard = keyboard_helper.create_back_keyboard()
            self.keyboard.send_keyboard(message.chat.id, lost_and_found_texts[button_type], keyboard)
            return lost_and_found_transitions[button_type]
        if button_type in [ButtonType.FoundList, ButtonType.LostList]:
            if button_type == ButtonType.FoundList:
                storage = self.found_items
            else:
                storage = self.lost_items
            items = storage.read_all()
            show_ids = user_info.access_level.value >= AccessLevel.PREP.value
            result = self._format_items(items, show_ids=show_ids) or "Пока еще ничего нет"
            self.bot.send_message(message.chat.id, result)
            return state

    def _handle_items(self, message):
        button_type = parser.try_parse_button(message.text)
        if button_type is None:
            return None
        if button_type == ButtonType.Back:
            self.keyboard.send_hide_keyboard(message)
            return State.MAIN
        if button_type == ButtonType.Lost:
            keyboard = keyboard_helper.create_lost_keyboard()
            self.keyboard.send_keyboard(message.chat.id, 'Выбери', keyboard)
            return State.LOST
        if button_type == ButtonType.Found:
            keyboard = keyboard_helper.create_found_keyboard()
            self.keyboard.send_keyboard(message.chat.id, 'Выбери', keyboard)
            return State.FOUND

    def send_keyboard_in_items(self, message: Message):
        keyboard = keyboard_helper.create_lost_and_found_keyboard()
        self.keyboard.send_keyboard(message.chat.id, 'Потеряшки и находки', keyboard)

    def _format_items(self, items, show_ids: bool) -> str:
        if show_ids:
            infos = [(self.users.read(item.user_id).name, item.item_id, item.info) for item in items]
            format_str = '{}, {}: {}'
        else:
            infos = [(self.users.read(item.user_id).name, item.info) for item in items]
            format_str = '{}: {}'
        return '\n'.join([format_str.format(*info) for info in infos])
